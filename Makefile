DESTDIR   := ~
PREFIX    := .local

all:

install:
	mkdir -p $(DESTDIR)/$(PREFIX)/bin
	install -m 0755 mrandr $(DESTDIR)/$(PREFIX)/bin/mrandr
	cp -f mrandr-xrandr-parser.sed $(DESTDIR)/$(PREFIX)/bin/

uninstall:
	rm -rf $(DESTDIR)/$(PREFIX)/bin/mrandr $(DESTDIR)/$(PREFIX)/bin/mrandr-xrandr-parser.sed
